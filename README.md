*This README would normally document whatever steps are necessary to get your application up and running.*

**Our teams repository is for Website development and collaboration**

Throughout the duration of the semester teams will be randomly formed to execute development for both websites and mobile applications. By using Bitbucket, the team's efficiency to work cohesively will increase and make collaboration easier.

**Contribution**

* Team leader: Joseph Patrick Giurintano
* Readme write-up: Cristian Zamarripa
* Code edits: everyone in team 2

**Who do I talk to?**

* If you have any questions or concerns please feel free to contact us directly (850) 282-3341 or through email Webdevgteam2@gmail.com

* Administrative team: Joseph Patrick Giurintano, Cristian Zamarripa, Adam Jankowski